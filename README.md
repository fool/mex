# mex

Measurable Execution Context

A helper to make execution contexts that are pluggable into metric systems

```scala
import net.foolz.mex.ThreadPoolExecutorBuilder
import scala.concurrent.ExecutionContext

val threadPoolSize = 2
val measurableExecutor = ThreadPoolExecutorBuilder(threadPoolSize, threadPoolSize).build()

implicit val ctx: ExecutionContext = measurableExecutor.executionContext

Future(someCode()) /** runs right away in ctx */
Future(someCode()) /** runs right away in ctx */
Future(someCode()) /** waits in the queue until a thread is available */
Future(someCode()) /** waits in the queue until a thread is available */
Future(someCode()) /** waits in the queue until a thread is available */
Future(someCode()) /** waits in the queue until a thread is available */
Future(someCode()) /** waits in the queue until a thread is available */

println(s"There's ${measurableExecutor.activeThreads} active threads and " +
        s"${measurableExecutor.queueSize} items waiting to execute in the queue")

```

## Supports
- ThreadPoolExecutor
- ForkJoinPool

With (optional) nifty builders to create those executors a little more easily