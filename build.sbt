
lazy val `mex` = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "net.foolz",
      scalaVersion := "2.12.3",
      version      := "0.1.0",
      crossScalaVersions := Seq("2.12.6")
    )),
    name := "mex",
    bintrayRepository := "mex",
    licenses += ("MIT", url("http://opensource.org/licenses/MIT")),
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
  )


import ReleaseTransformations._

releaseProcess := Seq[ReleaseStep](
checkSnapshotDependencies,              // : ReleaseStep
inquireVersions,                        // : ReleaseStep
runClean,                               // : ReleaseStep
runTest,                                // : ReleaseStep
setReleaseVersion,                      // : ReleaseStep
commitReleaseVersion,                   // : ReleaseStep, performs the initial git checks
tagRelease,                             // : ReleaseStep
publishArtifacts,                       // : ReleaseStep, checks whether `publishTo` is properly set up
setNextVersion,                         // : ReleaseStep
commitNextVersion,                      // : ReleaseStep
pushChanges                             // : ReleaseStep, also checks that an upstream branch is properly configured
)

