package net.foolz.mex

import java.lang.Thread.UncaughtExceptionHandler
import java.util.concurrent.ForkJoinPool
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory


case class ForkJoinPoolBuilder(
  parallelism: Option[Int] = None,
  threadFactory: Option[ForkJoinWorkerThreadFactory] = None,
  handler: Option[UncaughtExceptionHandler] = None,
  asyncMode: Option[Boolean] = None,
  reporter: Option[Throwable => Unit] = None
) {
  def withParallelism(parallelism: Int): ForkJoinPoolBuilder = {
    copy(parallelism = Some(parallelism))
  }

  def withThreadFactory(threadFactory: ForkJoinWorkerThreadFactory): ForkJoinPoolBuilder = {
    copy(threadFactory = Some(threadFactory))
  }

  def withHandler(handler: UncaughtExceptionHandler): ForkJoinPoolBuilder = {
    copy(handler = Some(handler))
  }

  def withAsyncMode(asyncMode: Boolean): ForkJoinPoolBuilder = {
    copy(asyncMode = Some(asyncMode))
  }

  def withReporter(reporter: Throwable => Unit): ForkJoinPoolBuilder = {
    copy(reporter = Some(reporter))
  }

  def build(): MeasurableForkJoinPoolContext = {
    MeasurableForkJoinPoolContext((parallelism, threadFactory, handler, asyncMode) match {
      case (Some(p), Some(t), Some(h), Some(a)) => new ForkJoinPool(p, t, h, a)
      case (Some(p), None, None, None)          => new ForkJoinPool(p)
      case (None, None, None, None)             => new ForkJoinPool()
      case _ => throw new IllegalArgumentException(s"There are three constructors to ForkJoinPool: no arguments, parallelism, and parallelism + thread factory + exception handler + async mode. I don't know which constructor to use")
    }, reporter)
  }
}