package net.foolz.mex

import java.util.concurrent.{Executor, ForkJoinPool, ThreadPoolExecutor}

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, ExecutionContextExecutorService}

/**
  * An execution context with a couple methods of insight. Unfortunately the 2 commonly used
  * java built in thread pools, ThreadPoolExecutor and ForkJoinPool both have different
  * functionality and offer different monitoring capabilities. It's recommended to use
  * the subclasses of MeasurableExecutionContext specific to the thread pool implementation
  * you are using to get the most value
  */
trait MeasurableExecutionContext {
  def executionContext: ExecutionContext
  def activeThreads: Int
  def queueSize: Int
}


case class MeasurableThreadPoolExecutorContext(
  executor: ThreadPoolExecutor,
  executionContext: ExecutionContext
) extends MeasurableExecutionContext {
  def activeThreads: Int = executor.getActiveCount
  def queueSize: Int = executor.getQueue.size()
  def remainingCapacity: Int = executor.getQueue.remainingCapacity()
}

object MeasurableThreadPoolExecutorContext {
  def apply(executor: ThreadPoolExecutor, reporter: Option[Throwable => Unit] = None): MeasurableThreadPoolExecutorContext = {
    MeasurableThreadPoolExecutorContext(executor, reporter match {
      case Some(r) => ExecutionContext.fromExecutorService(executor, r)
      case None => ExecutionContext.fromExecutorService(executor)
    })
  }
}

case class MeasurableForkJoinPoolContext(
  executor: ForkJoinPool,
  executionContext: ExecutionContext
) extends MeasurableExecutionContext {
  def activeThreads: Int = executor.getActiveThreadCount
  def queueSize: Int = executor.getQueuedSubmissionCount
}


object MeasurableForkJoinPoolContext {
  def apply(executor: ForkJoinPool, reporter: Option[Throwable => Unit] = None): MeasurableForkJoinPoolContext = {
    MeasurableForkJoinPoolContext(executor, reporter match {
      case Some(r) => ExecutionContext.fromExecutor(executor, r)
      case None => ExecutionContext.fromExecutor(executor)
    })
  }
}
