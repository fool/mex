package net.foolz.mex

import java.util.concurrent.ThreadFactory
import java.util.concurrent.atomic.AtomicLong

class NamedThreadFactory(name: String) extends ThreadFactory {
  val counter: AtomicLong = new AtomicLong()
  def newThread(r: Runnable): Thread = {
    val t = new Thread(r)
    t.setName(s"$name-${counter.incrementAndGet()}")
    t
  }
}
