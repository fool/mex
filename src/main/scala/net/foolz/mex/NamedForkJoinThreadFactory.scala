package net.foolz.mex

import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{ForkJoinPool, ForkJoinWorkerThread}

class NamedForkJoinThreadFactory(name: String) extends ForkJoinWorkerThreadFactory {
  val counter: AtomicLong = new AtomicLong()
  def newThread(pool: ForkJoinPool): ForkJoinWorkerThread = {
    new NamedForkJoinWorkerThread(s"$name-${counter.incrementAndGet()}", pool)
  }
}

class   NamedForkJoinWorkerThread(name: String, pool: ForkJoinPool)
extends ForkJoinWorkerThread(pool) {
  super.setName(name)
}
