package net.foolz.mex

import java.util.concurrent.{BlockingQueue, LinkedBlockingQueue, RejectedExecutionHandler, ThreadFactory, ThreadPoolExecutor, TimeUnit}

import scala.concurrent.duration.{Duration, FiniteDuration}


case class ThreadPoolExecutorBuilder(
  minSize: Int,
  maxSize: Int,
  keepAliveTimeout: Duration = new FiniteDuration(0, TimeUnit.NANOSECONDS),
  workQueue: BlockingQueue[Runnable] = new LinkedBlockingQueue[Runnable](),
  threadFactory: Option[ThreadFactory] = None,
  rejectionHandler: Option[RejectedExecutionHandler] = None,
  reporter: Option[Throwable => Unit] = None
) {
  def withMinSize(minSize: Int): ThreadPoolExecutorBuilder = {
    copy(minSize = minSize)
  }

  def withMaxSize(maxSize: Int): ThreadPoolExecutorBuilder = {
    copy(maxSize = maxSize)
  }

  def withKeepAliveTimeout(keepAliveTimeout: Duration): ThreadPoolExecutorBuilder = {
    copy(keepAliveTimeout = keepAliveTimeout)
  }

  def withWorkQueue(workQueue: BlockingQueue[Runnable]): ThreadPoolExecutorBuilder = {
    copy(workQueue = workQueue)
  }

  def withRejectionHandler(rejectionHandler: RejectedExecutionHandler): ThreadPoolExecutorBuilder = {
    copy(rejectionHandler = Some(rejectionHandler))
  }

  def withReporter(reporter: Throwable => Unit): ThreadPoolExecutorBuilder = {
    copy(reporter = Some(reporter))
  }

  def build(): MeasurableThreadPoolExecutorContext = {
    MeasurableThreadPoolExecutorContext((threadFactory, rejectionHandler) match {
      case (Some(factory), Some(handler)) => new ThreadPoolExecutor(minSize, maxSize, keepAliveTimeout.toNanos, TimeUnit.NANOSECONDS, workQueue, factory, handler)
      case (Some(factory), None)          => new ThreadPoolExecutor(minSize, maxSize, keepAliveTimeout.toNanos, TimeUnit.NANOSECONDS, workQueue, factory)
      case (None, Some(handler))          => new ThreadPoolExecutor(minSize, maxSize, keepAliveTimeout.toNanos, TimeUnit.NANOSECONDS, workQueue, handler)
      case (None, None)                   => new ThreadPoolExecutor(minSize, maxSize, keepAliveTimeout.toNanos, TimeUnit.NANOSECONDS, workQueue)
    }, reporter)
  }
}
