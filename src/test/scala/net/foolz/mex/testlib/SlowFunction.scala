package net.foolz.mex.testlib

trait SlowFunction {
  def slowFunction(seconds: Int): Unit = {
    Thread.sleep(seconds * 1000)
  }
}
