package net.foolz.mex.tests

import net.foolz.mex.ForkJoinPoolBuilder
import net.foolz.mex.testlib.SlowFunction
import org.scalatest.FlatSpec

import scala.concurrent.{ExecutionContext, Future}

class   ForkJoinPoolTest
extends FlatSpec
with    SlowFunction {
  s"Fork Join Pool Executor" should "run the code from the readme properly" in {
    val measurableExecutor = ForkJoinPoolBuilder().withParallelism(2).build()
    implicit val ctx: ExecutionContext = measurableExecutor.executionContext

    Future(slowFunction(5)) /** runs right away in ctx */
    Future(slowFunction(5)) /** runs right away in ctx */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */

    val active = measurableExecutor.activeThreads
    val queued = measurableExecutor.queueSize
    assertResult(2, s"Active threads should match min/max thread pool size")(active)
    assertResult(5, s"Queued threads should be all the rest of the futures")(queued)
  }
}
