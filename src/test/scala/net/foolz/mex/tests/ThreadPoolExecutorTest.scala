package net.foolz.mex.tests

import java.util.concurrent.ArrayBlockingQueue

import net.foolz.mex.ThreadPoolExecutorBuilder
import net.foolz.mex.testlib.SlowFunction
import org.scalatest.FlatSpec

import scala.concurrent.{ExecutionContext, Future}

class   ThreadPoolExecutorTest
extends FlatSpec
with    SlowFunction {
  s"Thread Pool Executor" should "run the code from the readme properly" in {
    val measurableExecutor = ThreadPoolExecutorBuilder(1, 1).build()
    implicit val ctx: ExecutionContext = measurableExecutor.executionContext

    Future(slowFunction(5)) /** runs right away in ctx */
    Future(slowFunction(5)) /** runs right away in ctx */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */

    val active = measurableExecutor.activeThreads
    val queued = measurableExecutor.queueSize
    assertResult(1, s"Active threads should match min/max thread pool size")(active)
    assertResult(6, s"Queued threads should be all the rest of the futures")(queued)
  }

  s"Thread Pool Executor" should "report remaining capacity" in {
    val measurableExecutor = ThreadPoolExecutorBuilder(2, 2)
      .withWorkQueue(new ArrayBlockingQueue[Runnable](10))
      .build()
    implicit val ctx: ExecutionContext = measurableExecutor.executionContext

    Future(slowFunction(5)) /** runs right away in ctx */
    Future(slowFunction(5)) /** runs right away in ctx */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */
    Future(slowFunction(5)) /** waits in the queue until a thread is available */

    val remaining = measurableExecutor.remainingCapacity
    assertResult(5, s"Half the queue should be used up")(remaining)
  }
}