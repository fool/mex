

compile:
	sbt compile || (stty sane & exit 1)
	stty sane

test:
	sbt test || (stty sane & exit 1)
	stty sane

# install current locally
build:
	sbt publishLocal || (stty sane & exit 1)
	stty sane

# push current commit to bintry
bintray:
	sbt '+publish' || (stty sane & exit 1)
	stty sane

# push to bintray and remote repo
release:
	sbt '+release' || (stty sane & exit 1)
	stty sane

